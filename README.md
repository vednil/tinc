# Quick reference

- Maintained by: https://gitlab.com/vednil/tinc

# What is it?

tinc is a VPN (Virtual Private Network) software.

# How to use this image

Run tinc on server with public IP:
```
$ docker run -d \
        --name=tinc \
        --net=host \
        --device=/dev/net/tun \
        --cap-add=NET_ADMIN \
        -e HOST=server \
        -e PUBLIC_IP=1.2.3.4 \
        -e PRIVATE_IP=192.168.1.1 \
        -e PRIVATE_NETWORK=192.168.1.0/24 \
        -v tinc:/etc/tinc \
        vednil/tinc
```

Run without public IP and connect to remote server:
```
$ docker run -d \
        --name=tinc \
        --net=host \
        --device=/dev/net/tun \
        --cap-add=NET_ADMIN \
        -e HOST=client \
        -e PRIVATE_IP=192.168.1.2 \
        -e PRIVATE_NETWORK=192.168.1.0/24 \
        -e REMOTE_HOST=server \
        -e REMOTE_CONFIG=QWRkcmVzcyA9IDEuMi4zLjQKU3VibmV0ID0gMTkyLjE2OC4xLjEKCi0tLS0tQkVHSU4gUlNBIFBVQkxJQyBLRVktLS0tLQpNSUlDQ2dLQ0FnRUFtUGVwTmtwYVhrQVRHMEhwRmpnM3JOcDIzWjFXZDM5Rk5lY3BFRXFZVTFWc3dxYzVUZmg3CkhkNWNmOTZ1Vk84QXZvbzhKSTh0NmhCOU85b3pZUTB3bXpqU0xBRkpCUE5SZElpZkRVQ2w0WmV5NStsaFFFVGgKSFVlMTZvNG81akN3bmdhK0lsY2pUVFRlMjFHRzdoVDFCNytyLy9IN2J5eVM4K0RDb0hoc25ReWRMUmtoOGlVNwpURUZmL3dhZ0RqeTR4R0NXTVFhUzFELzBNaE1OWU5OT2Z5QkVuUkhSY1RSdWQvOUo3azRHSWtyK2dpRitqcm1DClRqZ01sTEJaSTVnMnJyN3p1WGZQQS9lcmFrakluTjZSOU5Cb09QcXNNSEh4QkRsQVk2Y3dHUDNXMFdOQ3E2blAKdU91bEIrU05XWTIxV1pwS3NGM3EvL2xqKzJzdi9EbndVcFBrUmVISU5mMWNQUGIyQk5uVy9ad0UvOTVGS2VsbAp0ZXJHYWR5Q3BJeUkrc2FNMGhWcWh3Nm5hYzdrUWwxOWE3Q3NkZXdTTHZUWDZIQlRienZYNFhjaEFNYk1TdTlsCnlBT2twbEVCcStYbW1YVFZOdzk5RUJ3Q25zbG1hOUF0KzdTaFdDYWlHRW9DWjYxWTRseUx0aFFmTDM1SlV3NHIKb2kzRmJLblpnWWRhUXZMS0t2eC9pcmVxVXkxcFVQUWN0Z21pMUNkcWU0Tm5SWDZuNzhLVVpIOU4rVHk4bWw1YQpnM3pSUEpqWnFkWjZMcCtLdzUrVlpjL2Zwcy9rVi9kajJWOTlacDhKc0pRZ1BiUjBmZzE3K0xKQ3FodDg4ZXhECmRGNXRNSGhCSDdkTEZxVHFwdXpnMGxQMGU1NHpETFY0ZjlXZm9kVXgzVXB1VFROcno3eUdVQjBDQXdFQUFRPT0KLS0tLS1FTkQgUlNBIFBVQkxJQyBLRVktLS0tLQo= \
        -v tinc:/etc/tinc \
        vednil/tinc
```

**Note:** Exec `docker exec tinc bash -c 'cat /etc/tinc/hosts/${HOST} | base64 -w 0'` command on tinc server with public IP address to get REMOTE_CONFIG variable value for server without public IP address.

## Environment Variables

### `HOST`

Server name. Use only alfanumeric and underscore characters (a-z, A-Z, 0-9 and _).

### `PUBLIC_IP`

Server public IP address available through the Internet.

### `PRIVATE_IP`

tinc server private IP address.

### `PRIVATE_NETWORK`

tinc private network.

### `REMOTE_HOST`

Remote tinc server public IP address.

### `REMOTE_CONFIG`

Remote tinc server base64 encoded config.

