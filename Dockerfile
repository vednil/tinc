FROM ubuntu:22.04

# Install tinc
RUN apt-get update \
 && apt-get install -y tinc iproute2 \
 && rm -rf /var/lib/apt/lists/*

VOLUME /etc/tinc

COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 655 655/udp

CMD tincd -D
