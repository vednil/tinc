#!/bin/bash

# Create hosts directory
if [ ! -d /etc/tinc/hosts ]
then
    mkdir -p /etc/tinc/hosts
fi

# Create main config
if [ ! -f /etc/tinc/tinc.conf ]
then

    echo "Name = ${HOST}" > /etc/tinc/tinc.conf
    echo "Interface = tun0" >> /etc/tinc/tinc.conf

    if [ ! -z "${REMOTE_HOST}" ] && [ ! -z "${REMOTE_CONFIG}" ]
    then
        echo "ConnectTo = ${REMOTE_HOST}" >> /etc/tinc/tinc.conf
        # Add remote host config
        echo ${REMOTE_CONFIG} | base64 -d > /etc/tinc/hosts/${REMOTE_HOST}
    fi

fi

# Create host config
if [ ! -f /etc/tinc/hosts/${HOST} ]
then

    if [ ! -z "${PUBLIC_IP}" ]
    then
        echo "Address = ${PUBLIC_IP}" > /etc/tinc/hosts/${HOST}
    fi
    echo "Subnet = ${PRIVATE_IP}" >> /etc/tinc/hosts/${HOST}

    # Generate RSA key
    if [ ! -f /etc/tinc/rsa_key.priv ]
    then
        tincd -K 4096 < /dev/null
    fi

    HOST_CONFIG=$(cat /etc/tinc/hosts/${HOST} | base64 -w 0)
    echo
    echo "Add created config to remote tinc server:"
    echo "\$ docker exec tinc bash -c 'echo ${HOST_CONFIG} | base64 -d > /etc/tinc/hosts/${HOST}'"
    echo

fi

# Add scripts
cat > /etc/tinc/tinc-up <<EOF
#!/bin/sh
ip link set \$INTERFACE up
ip addr add ${PRIVATE_IP} dev \$INTERFACE
ip route add ${PRIVATE_NETWORK} dev \$INTERFACE
EOF

cat > /etc/tinc/tinc-down <<EOF
#!/bin/sh
ip route del ${PRIVATE_NETWORK} dev \$INTERFACE
ip addr del ${PRIVATE_IP} dev \$INTERFACE
ip link set \$INTERFACE down
EOF

chmod 755 /etc/tinc/tinc-up
chmod 755 /etc/tinc/tinc-down


exec "$@"
